int notes[] = {440, 554, 660, 880};


void setup() {
  Serial.begin(9600);
}


void loop() {
  int keyVal = analogRead(A0);
  Serial.println(keyVal);

  int note = play(keyVal);
  
  if (note != 0) {
    tone(8, note);
  }
  else {
    noTone(8);
  }
}


int play(int val) {
  int note = 0;
  
  if (val == 1023) {
    note = notes[0]; 
  }
  else if (val >= 990 && val <= 1010) {
    note = notes[1];
  }
  else if (val >= 505 && val <= 515) {
    note = notes[2];
  }
  else if (val >= 5 && val <= 10) {
    note = notes[3];
  }

  return note;
}








